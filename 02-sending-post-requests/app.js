const express = require("express");
const bodyParser = require("body-parser");
const path = require('path');
const multer=require('multer');
const feedRoutes = require("./routes/feed");
const authRoutes = require("./routes/auth");
const app = express();
const { check, validationResult } = require("express-validator");
// app.use(bodyParser.urlencoded()); // x-www-form-urlencoded <form>
app.use(bodyParser.json()); // application/json

app.use(multer().single('image'))
app.use(express.static(path.join(__dirname, 'public')));
app.use(
  [check("title").isLength({ min: 3 }), check("content").isLength({ min: 4 })],
  (req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
      "Access-Control-Allow-Methods",
      "OPTIONS, GET, POST, PUT, PATCH, DELETE"
    );
    res.setHeader(
      "Access-Control-Allow-Headers",
      "Content-Type, Authorization"
    );
    next();
  }
);

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*"),
    res.setHeader(
      "Access-Control-Allow-Methods",
      "OPTIONS,GET,POST,PUT,DELETE"
    );
    next();
});

app.use("/feed", feedRoutes);
app.use("/auth", authRoutes);
app.listen(8080);
