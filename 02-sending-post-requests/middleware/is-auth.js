const jwt=require('jsonwebtoken');

module.exports=(req,res,next)=>{
    const authHeader=req.get('Authorization');
    if(!authHeader){
        const error=new Error('Not authenticated. ');
        error.statusCode=401;
        throw console.error();
    }
    const token=req.get('Authorization').split(' ')[1];
    //console.log(token,req);
    let decodedToken;
    try{
        decodedToken=jwt.verify(token,'secretUperUser');        
    }
    catch(error){
        error.statusCode=500;
        throw error;
    }
    if(!decodedToken){
        const error=new Error('Not authenticated');
        error.statusCode=400;
        throw error;
    }
    req.userId=decodedToken.userId;
    next()
}