const User = require("../Model/user");

const bcrypt = require("bcryptjs");

const jwt = require("jsonwebtoken");


const nodemailer=require('nodemailer');

const sendGridTransport=require('nodemailer-sendgrid-transport');

const transporter=nodemailer.createTransport(sendGridTransport({
  auth:{
    api_key:'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'
  }
}))

const { check, validationResult } = require("express-validator");

exports.signup = async (req, res, next) => {
  const email = req.body.email;
  const name = req.body.name;
  const password = req.body.password;
  let existEmail;
  await User.findOne({ email: email }).then((item) => {
    //console.log(item);
    if (item) {
      existEmail = item.email;
      //console.log(existEmail);
    }
  });
  //console.log(existEmail, "===========>");
  if (existEmail) {
    return res.status(422).json({ message: "Email already exists!" });
    //console.log(User.findOne({email:email}).then(res=> res.email));
  } else {
    bcrypt
      .hash(password, 12)
      .then((hashedPw) => {
        const user = new User({
          email: email,
          password: hashedPw,
          name: name,
        });

        return user.save();
      })
      .then((result) => {
        res.status(201).json({ message: "User created", userId: result._id });
        return transporter.sendMail({
          to:email,
          from:'praveen2017mishra@gmail.com',
          subject:'Signup successfully',
          html:'<h1>Successfully signup </h1>'
        })
        
      }).catch(err=>{
        console.log(err);
      });
    //.catch((error) => res.status(422).json({ message: "Database errror" }));
  }
};

exports.login = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  let loadUser;
  User.findOne({ email: email })
    .then((user) => {
      if (!user) {
        const error = new Error(
          "A Error user with this email could not be found"
        );
        error.statusCode = 401;
        throw error;
      }
      loadUser = user;
      return bcrypt.compare(password, user.password);
    })
    .then((isEqual) => {
      //console.log(isEqual);
      if (!isEqual) {
        const error = new Error("Wrong password");
        error.statusCode = 401;
        throw error;
      }
      const token = jwt.sign(
        { email: loadUser.email, userId: loadUser._id.toString() },
        "secretUperUser",
        { expiresIn: "1h" }
      );
      res.status(200).json({ token: token, userId: loadUser._id.toString() });
    })
    .catch((error) => {
      if (!error.statusCode) {
        error.statusCode = 500;
      }
      next(error);
    });
};
