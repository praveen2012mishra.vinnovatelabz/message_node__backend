const { check, validationResult } = require("express-validator");
let Product = require("../Model/DB");



exports.getImage=(req,res,next)=>{
  //console.log(req,res,next);
  console.log((req.file));
}


exports.getPosts = (req, res, next) => {
  //const product=new Product();
  Product.find();
  const currentPage = req.query.page || 1;
  const perPage = 2;
  let totalPage;
  Product.find()
    .countDocuments()
    .then((count) => {
      totalPage = count;
      //console.log(Product.find().skip((currentPage - 1) * perPage));
      return Product.find()
        .skip((currentPage - 1) * perPage)
        .limit(perPage);
    })
    .then((item) => {
      //console.log('hi i am praveen',item);
      res.status(200).json({
        posts: item,
        totalItems: totalPage,
      });
    });
  Product.find();
};

exports.createPost = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  const title = req.body.title;
  const content = req.body.content;
  let newObject = Object.assign(
    {},
    {
      title: title,
      content: content,
      imageUrl: "images/duck.jpg",
      creator: {
        name: "Maximilian",
      },
      date: new Date(),
    }
  );
  //console.log(newObject);
  const product = new Product(newObject);
  product.save();
  // Create post in db
  res.status(201).json({
    message: "Post created successfully!",
    post: { id: new Date().toISOString(), title: title, content: content },
  });
};
