const express = require("express");

const { check, validationResult } = require("express-validator");

const User = require("../Model/user");

const authController = require("../controllers/auth");

const router = express.Router();

router.put(
  "/signup",
  authController.signup
);

router.post('/login',authController.login);

module.exports = router;
