const express = require("express");
const { check, validationResult } = require("express-validator");

const feedController = require("../controllers/feed");

const isAuth = require("../middleware/is-auth");

const router = express.Router();

const path = require("path");
const multer = require("multer");

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
  cb(null, 'public')
},
filename: function (req, file, cb) {
  cb(null, Date.now() + '-' +file.originalname )
}
})

var upload = multer({ storage: storage }).single('file')

// router.use(express.static(path.join(__dirname,'public')));

// GET /feed/posts
router.get("/posts", isAuth, feedController.getPosts);

router.post('/image',function(req, res) {
     
  upload(req, res, function (err) {
         if (err instanceof multer.MulterError) {
             return res.status(500).json(err)
         } else if (err) {
             return res.status(500).json(err)
         }
         console.log(req.file);
    return res.status(200).send('hi broo praveen')

  })
  //return res.status(200).send('hi broo praveen')
});


// POST /feed/post
router.post(
  "/post", isAuth,
  [
    check("content")
      .isLength({ min: 3 })
      .withMessage("Name must have more than 5 characters"),
    check("content").isLength({ min: 3 }),
  ],
  feedController.createPost
);

module.exports = router;
